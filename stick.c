/* Griffen Agnello	CSE224		HW004		10/15/20
This is the stick game implemented into C. The user is able to execute "stick" followed by a number. The inputted number will be the total number of sticks
in the pile. If the user does not enter a number, the program will ask the user how many sticks they want to play with. If the user incorretly inputs a
number for the stick pile, the program will close. If the user inputs a correct choice, the program will then show the total number of sticks in the pile. 
The program will ask the user to draw 1-4 sticks. If the user incorrectly inputs a choice, the question will be asked again. The program will subtract 1-4
sticks from the total number, display the total number of sticks, and then checks the victory condition. If the victory condition returns a "0", the 
game continues. The computer's turn starts, and the computer will divide the total number of sticks by 5, then subtract from the pile by the remainder of
the divison. The victory condition runs again. Assuming all the sticks are taken and the total number equals exactly 0, the victory condition will be
set to "1". At this point, the player who took the last sticks will be declared the winner, and the program will close. The game runs in a loop
until either the user or the computer wins the game. */

#include <stdio.h>
#include <stdlib.h>
int userTurn(int *numsticks);	//Function declarations and library inclusions.
int cpTurn(int *numsticks);
void displaySticks(int *numsticks);
int victoryCondition(int *numsticks);

//The main function accepts arguments from the command line. The first user input will determine the total number of sticks.
int main(int argc, char *argv[])
{
	int numberOfSticks, playerStatus, result;	//numberOfSticks is the total number of sticks in the pile.
	char numberOfSticksInput[30];			//If the user does not input a command line argument, this variable will hold the
							// user input for the total number of sticks in the pile.
	if(argc > 2)	{						
		printf("ERROR, only 1 argument is accepted.\n");	//Detects if there is more than 1 argument. If so, the program closes.
		exit(0);
	}
	printf("Howdy gamer, I hope you're ready to have the time of your life!\n");	//Greet user
	
	if(argv[1] != NULL)	{	//Detects if the user inputted an argument for the number of sticks.
		result = sscanf(argv[1], "%d", &numberOfSticks);	//Searches for a valid integer (greater than 9) in the 
									//second argument from the command line
		if(result != 1 || numberOfSticks < 10)	{
			if (numberOfSticks < 10)	{
				printf("Wait nevermind, I change my mind. You messed up, Goodbye. :)\n");
				exit (0);		//If the number is less than 10, the program exits.
			}
		}
	}
	else	{
		printf("Enter how many sticks you want to play with. Greater than 9 please: ");
		fgets(numberOfSticksInput,30,stdin);	//If argument was detected, the program asks the user for an input.

		while (1 != sscanf(numberOfSticksInput, "%d", &numberOfSticks))	//Scans if the input is legal and greater than 9.
		{								// If not, the program stops.
			printf("Sorry mate, we don't do that kind of thing around here... I'm going to go now, bye!\n");
			exit (0);
		}
		if (numberOfSticks < 10){
			printf("That's not how this works. Begone.\n");
			exit (0);
		}
	}
	printf("\nYou know, %d is my favorite number. Yup.\n", numberOfSticks);	//fun!
	displaySticks(&numberOfSticks);		//Calls the "displaySticks" function and accepts a pointer to the value of numberOfSticks.
	while (2020 == 2020) {			// This function will display the initial number of sticks.
		playerStatus = userTurn(&numberOfSticks);			//Variable playerStatus will hold an integer from cpTurn and userTurn.
		if (playerStatus == 1) {					//The userTurn and cpTurn functions will return a number 0-2,
			printf("You won! Congratulations. You're awesome.\n");	//This number will determine if the game should continue or not.
			exit (0);						//A "1" returned from the userTurn will annouce that the user has won.
			break;							//A "2" from the user or a "1" from cpTurn will announce user defeat.
		}								
		if (playerStatus == 2) {
			printf("Oh no, you lost. Oops.\n");
			exit (0);
			break;
		}
		playerStatus = cpTurn(&numberOfSticks);			//If the user has not won or lost, the computer's turn will begin.
		if (playerStatus == 1) {				//If either turn returns a "0", the game will continue. The game
			printf("Oh no. You lost. Oops\n");		//continues in an infinite loop until one player wins.
			exit (0);
			break;
		}
	}				 
	return 0;
}

//This function displays the total number of sticks remaining in the pile. This function accepts a pointer to the value of the number of sticks remaining
void displaySticks(int *numberOfSticks)
{
	int counter=0;							//"counter" keeps track of the number of sticks in the pile. It is initialized
	printf("\nThere are this many sticks left in the pile.\n");	//To "0" so that whenever the function is ran, the counter will count the current
	while(counter < *numberOfSticks){				//number of sticks. Counter will increment by 1 and simultaneously print a pipe
		counter++;						//in a loop. When the counter reaches the number of sticks, the loop ends and the
		printf("|");						//current number is printed in parenthesis.
	}
	printf("(%d)\n", *numberOfSticks);
	return;
}

//This function runs inside the cpTurn and userTurn functions. This function checks the number of sticks left and will return the appropriate integer.
int victoryCondition(int *numberOfSticks)
{
	if ( *numberOfSticks == 0) {		//If the number of Sticks equals exactly 0, then a "1" is returned, meaning a player has won.
		return 1; 
	}
	if (*numberOfSticks > 0) {		//If the number of sticks is greater than 0, then a "0" is returned, meaning the game continues.
		return 0; 
	}
	if (*numberOfSticks < 0) {		//If the number of sticks is less than 0 (user self-destruct), then a "2" is returned, meaning the user loses.
		return 2; 
	}
}

//This function starts the users turn.
int userTurn(int *numberOfSticks)
{
	char sticksUserTakesInput[30];
	int sticksUserTakes, userStatus;		//The program will start by asking the user to draw 1-4 sticks from the pile.
	printf("Please enter a number from 1-4. Do not enter anything else. Please just don't: ");
	fgets(sticksUserTakesInput, 30, stdin);		//The input is stored in this string variable so it may be parsed.
	while (1 != sscanf(sticksUserTakesInput, "%d", &sticksUserTakes) || 1 == sscanf(sticksUserTakesInput, "%d", &sticksUserTakes)) {
		if (sticksUserTakes > 0 && sticksUserTakes < 5)		{	//If sscanf returns a 1 AND the user input is from 1-4, the while loop ends.
			break;							//If sscanf returns a -1 (illegal input or not between 1-4), the program
		}								//will ask the user again for an input. The loop continues until a legal input
		else {								//is made.
			printf("I literally just said don't do that. Please enter either a 1,2,3, or 4!: ");
			fgets(sticksUserTakesInput, 30, stdin);
		}
	}
	*numberOfSticks = *numberOfSticks - sticksUserTakes;		//Subtracts 1-4 sticks from the pile based on whatever the user inputted.
	printf("You take (%d) stick(s) from the pile.", sticksUserTakes);
	displaySticks(numberOfSticks);	//Shows the new number of sticks left.
	userStatus = victoryCondition(numberOfSticks);	//Checks the victory condition and returns a 0,1, or 2 to the main function.
	return userStatus;
}

//This function starts the computer's turn.
int cpTurn(int *numberOfSticks)
{
	int tempSticks = *numberOfSticks % 5;	//Divides the current number of sticks by 5 and stores the remainder into "tempSticks".
	int cpStatus;
	if (tempSticks == 0) {		//If the remainer is "0", the computer will subtract 1 stick by default from the pile.
		printf("The computer takes (1) stick from the pile.");
		*numberOfSticks = *numberOfSticks - 1;
	}
	else {
		printf("The computer takes (%d) stick(s) from the pile.", tempSticks);
		*numberOfSticks = *numberOfSticks - tempSticks;		//If there is a non-zero remainder, the computer will subtract that number from the pile.
	}
	displaySticks(numberOfSticks);					//Displays the new number of sticks remaining.
	cpStatus = victoryCondition(numberOfSticks);			//Checks the victory condition and returns a 0 or 1 to the main function.
	return cpStatus;
}									//End of code, thanks for reading!
